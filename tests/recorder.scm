;; Copyright (C) 2020 "AZ Company Group" LLC <https://gkaz.ru/>
;; Copyright (C) 2020 Artyom V. Poptsov <a@gkaz.ru>
;;
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

(use-modules (srfi srfi-64)
             (srfi srfi-26)
             (oop  goops)
             (finger-frame recorder))

(test-begin "recorder")


(test-equal "recorder: Make a recorder, check ffmpeg command"
  (list "/usr/bin/ffmpeg"
        "-video_size" "4x10"
        "-framerate"  "10"
        "-f"          "x11grab"
        "-grab_x"     "1"
        "-grab_y"     "2"
        "-i"          ":1"
        "-pix_fmt"    "yuv420p"
        "-vcodec"     "libx264"
        "-preset"     "veryfast"
        "-loglevel"   "panic"
        "-async"      "1"
        "-vsync"      "1"
        "./video.mp4")
  (let ((r (make <recorder>
             #:frame-rate       10
             #:frame-size       '(5 . 10)
             #:frame-position   '(1 . 2)
             #:output-file-name "./video.mp4"
             #:display ":1")))
    (recorder-make-command r)))


(test-end "recorder")

(exit (= (test-runner-fail-count (test-runner-current)) 0))

;;; recorder.scm ends here.
