;; Copyright (C) 2020 "AZ Company Group" LLC <https://gkaz.ru/>
;; Copyright (C) 2020 Artyom V. Poptsov <a@gkaz.ru>
;;
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

(use-modules (srfi srfi-64)
             (srfi srfi-26)
             (finger-frame fs))

(test-begin "fs")


(test-assert "directory?: A path"
  (directory? "/path/to/directory/"))

(test-assert "directory?: An exact file name"
  (not (directory? "/path/to/file.mp4")))

(test-assert "directory?: Current directory"
  (directory? "."))

(test-error "directory?: An empty string"
  #t
  (directory? ""))


(test-end "fs")

(exit (= (test-runner-fail-count (test-runner-current)) 0))

;;; output-file.scm ends here.
