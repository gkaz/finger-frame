(define-module (finger-frame window)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-11)
  #:use-module (oop goops)
  #:export (<window>
            read-all
            window-id
            window-name
            window-absolute-position
            window-relative-position
            window-dimension
            xwininfo
            xwininfo-output->window))

(define (append-1 lst elem)
  (append lst (list elem)))

(define (read-all port)
  (let loop ((line (read-line port 'trim))
             (result '()))
    (if (eof-object? line)
        result
        (loop (read-line port 'trim)
              (append-1 result line)))))


(define-class <window> ()
  ;; <number>
  (id
   #:init-keyword #:id
   #:accessor     window-id
   #:setter       window-id-set!
   #:init-value   0)

  ;; <string>
  (name
   #:init-keyword #:name
   #:accessor     window-name
   #:setter       window-name-set!
   #:init-value   "")

  ;; <pair>
  ;;
  ;; x, y coordinates of the upper left corner of a window.
  (absolute-position
   #:init-keyword #:absolute-position
   #:accessor     window-absolute-position
   #:setter       window-absolute-position-set!
   #:init-value   '(#f . #f))

  ;; <pair>
  ;;
  ;; x, y coordinates of the bottom right corner of a window.
  (relative-position
   #:init-keyword #:relative-position
   #:accessor     window-relative-position
   #:setter       window-relative-position-set!
   #:init-value   '(#f . #f))

  ;; <pair>
  (dimension
   #:init-keyword #:dimension
   #:accessor     window-dimension
   #:setter       window-dimension-set!
   #:init-value   '(#f . #f)))


(define (xwininfo)
  (let ((pipe (open-input-pipe "xwininfo -int")))
    (read-all pipe)))


;;;; Helper procedures to parse lines of 'xwininfo' output.

(define (xwininfo-parse-window-id str)
  (write str)
  (newline)
  (let* ((m    (string-match "xwininfo: Window id: ([0-9]+) \"(.*)\"" str))
         (id   (string->number (match:substring m 1)))
         (name (match:substring m 2)))
    (values id name)))

(define (xwininfo-parse-absolute-upper-left-x str)
  (let ((m (string-match ".*Absolute upper-left X:[^0-9]+([0-9]+).*" str)))
    (string->number (match:substring m 1))))

(define (xwininfo-parse-absolute-upper-left-y str)
  (let ((m (string-match ".*Absolute upper-left Y:[^0-9]+([0-9]+).*" str)))
    (string->number (match:substring m 1))))

(define (xwininfo-parse-relative-upper-left-x str)
  (let ((m (string-match ".*Relative upper-left X:[^0-9]+([0-9]+).*" str)))
    (string->number (match:substring m 1))))

(define (xwininfo-parse-relative-upper-left-y str)
  (let ((m (string-match ".*Relative upper-left Y:[^0-9]+([0-9]+).*" str)))
    (string->number (match:substring m 1))))

(define (xwininfo-parse-width str)
  (let ((m (string-match ".*Width:[^0-9]+([0-9]+).*" str)))
    (string->number (match:substring m 1))))

(define (xwininfo-parse-height str)
  (let ((m (string-match ".*Height:[^0-9]+([0-9]+).*" str)))
    (string->number (match:substring m 1))))


(define (xwininfo-output->window output)
  (let ((window (make <window>)))
    (for-each (lambda (elem)
                (define (contains str)
                  (string-contains elem str))
                (cond
                 ((contains "xwininfo: Window id:")
                  (let-values (((id name) (xwininfo-parse-window-id elem)))
                    (window-id-set! window id)
                    (window-name-set! window name)))
                 ((contains "Absolute upper-left X:")
                  (let ((x   (xwininfo-parse-absolute-upper-left-x elem))
                        (pos (window-absolute-position window)))
                    (window-absolute-position-set! window (cons x (cdr pos)))))
                 ((contains "Absolute upper-left Y:")
                  (let ((y   (xwininfo-parse-absolute-upper-left-y elem))
                        (pos (window-absolute-position window)))
                    (window-absolute-position-set! window (cons (car pos) y))))
                 ((contains "Relative upper-left X:")
                  (let ((x   (xwininfo-parse-relative-upper-left-x elem))
                        (pos (window-relative-position window)))
                    (window-relative-position-set! window (cons x (cdr pos)))))
                 ((contains "Relative upper-left Y:")
                  (let ((y   (xwininfo-parse-relative-upper-left-y elem))
                        (pos (window-relative-position window)))
                    (window-relative-position-set! window (cons (car pos) y))))
                 ((contains "Width:")
                  (let ((width (xwininfo-parse-width elem))
                        (dim   (window-dimension window)))
                    (window-dimension-set! window (cons width (cdr dim)))))
                 ((contains "Height:")
                  (let ((height (xwininfo-parse-height elem))
                        (dim    (window-dimension window)))
                    (window-dimension-set! window (cons (car dim) height))))))
              output)
    window))
