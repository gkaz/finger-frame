(define-module (finger-frame pulseaudio)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (oop goops)
  #:export (pactl-list-sources
            pactl-get-output-source))


;; TODO: Place the procedures to a separate module.

(define (append-1 lst elem)
  (append lst (list elem)))

(define (read-all port)
  (let loop ((line (read-line port 'trim))
             (result '()))
    (if (eof-object? line)
        result
        (loop (read-line port 'trim)
              (append-1 result line)))))


(define-method (pactl-list-sources)
  (let* ((pipe   (open-input-pipe "pactl list short sources"))
         (output (read-all pipe)))
    (map (lambda (source)
           (string-split source #\tab))
         output)))

(define-method (pactl-get-output-source)
  (let ((filtered (filter (lambda (source)
                            (string-contains (cadr source) "output"))
                          (pactl-list-sources))))
    (display filtered)
    (newline)
    (cadar filtered)))
