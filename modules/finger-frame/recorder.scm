(define-module (finger-frame recorder)
  #:use-module (oop goops)
  #:export (<recorder>
            recorder-frame-rate
            recorder-frame-size
            recorder-frame-position
            recorder-output-file-name
            recorder-pid
            recorder-make-command
            recorder-start!
            recorder-stop!
            recorder-running?))


;;;; Constants.

(define %ffmpeg-binary "/usr/bin/ffmpeg")


(define-class <recorder> ()
  ;; <boolean>
  (debug-mode?
   #:accessor     recorder-debug-mode?
   #:init-keyword #:debug-mode?
   #:init-value   #f)

  ;; <number>
  (frame-rate
   #:accessor     recorder-frame-rate
   #:init-keyword #:frame-rate
   #:init-value   15)

  ;; <pair>
  (frame-size
   #:accessor     recorder-frame-size
   #:init-keyword #:frame-size
   #:init-value   '(0 . 0))

  ;; <pair>
  (frame-position
   #:accessor     recorder-frame-position
   #:init-keyword #:frame-position
   #:init-value   '(0 . 0))

  (audio-playback-device
   #:accessor     recorder-audio-playback-device
   #:init-keyword #:audio-playback-device
   #:init-value   #f)

  ;; <string>
  (audio-input-device
   #:accessor     recorder-audio-input-device
   #:init-keyword #:audio-input-device
   #:init-value   #f)

  ;; <string>
  (output-file-name
   #:accessor     recorder-output-file-name
   #:init-keyword #:output-file-name
   #:init-value   "")

  ;; <string>
  (display
   #:accessor     recorder-display
   #:init-keyword #:display
   #:init-value   (getenv "DISPLAY"))

  ;; <number>
  (pid
   #:accessor     recorder-pid
   #:setter       recorder-pid-set!
   #:init-value   #f))



(define-method (recorder-running? (recorder <recorder>))
  (not (equal? (recorder-pid recorder) #f)))

(define-method (recorder-make-command (recorder <recorder>))
  `(,%ffmpeg-binary
    ,@(if (recorder-audio-playback-device recorder)
          (list "-f" "pulse"
                "-thread_queue_size" "1024"
                "-i" (recorder-audio-playback-device recorder)
                "-codec:a" "pcm_s16le")
          (list))
    ,@(if (recorder-audio-input-device recorder)
          (list "-f" "alsa"
                "-thread_queue_size" "1024"
                "-i" (recorder-audio-input-device recorder))
          (list))
    ,@(if (and (recorder-audio-playback-device recorder)
               (recorder-audio-input-device recorder))
          (list "-filter_complex" "amix=inputs=2:duration=longest"
                "-ac" "2")
          (list))
    "-video_size"
    ,(let* ((size (recorder-frame-size recorder))
            (w    (car size))
            (h    (cdr size)))
       (format #f "~ax~a"
               (if (odd? w)
                   (- w 1)
                   w)
               (if (odd? h)
                   (- h 1)
                   h)))
    "-framerate" ,(number->string
                   (recorder-frame-rate recorder))
    "-f" "x11grab"
    "-grab_x" ,(number->string
                (car (recorder-frame-position recorder)))
    "-grab_y" ,(number->string
                (cdr (recorder-frame-position recorder)))
    "-i" ,(recorder-display recorder)
    "-pix_fmt" "yuv420p"
    "-vcodec" "libx264"
    "-preset" "veryfast"
    "-loglevel" ,(if (recorder-debug-mode? recorder)
                     "verbose"
                     "panic")
    "-async" "1"
    "-vsync" "1"
    ,(recorder-output-file-name recorder)))

(define-method (recorder-start! (recorder <recorder>))
  (let ((pid (primitive-fork)))
    (cond
     ((zero? pid)
      (let ((cmd (recorder-make-command recorder)))
        (when (recorder-debug-mode? recorder)
          (format #t "ffmpeg command: ~a~%" cmd))
        (apply execle %ffmpeg-binary (environ) cmd)))
     ((> pid 0)
      (recorder-pid-set! recorder pid))
     (else
      (error "Could not start the recorder")))))

(define-method (recorder-stop! (recorder <recorder>))
  (kill (recorder-pid recorder) SIGINT)
  (waitpid (recorder-pid recorder))
  (recorder-pid-set! recorder #f))

;;;; recorder.scm ends here.

