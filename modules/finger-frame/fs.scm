(define-module (finger-frame fs)
  #:use-module (oop goops)
  #:export (generate-file-name
            directory?))

(define-method (generate-file-name (path <string>) (format <string>))
  (let ((timestamp (strftime "%FT%H%M%S" (localtime (current-time)))))
    (string-append path "/finger-frame-" timestamp "." format)))

(define-method (directory? (path <string>))
  (when (string-null? path)
    (error "Path is empty string" path))
  (let ((bn (basename path)))
    (or (string=? bn ".")
        (equal? (string-ref bn (- (string-length bn) 1)) #\/))))
