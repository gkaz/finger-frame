(define-module (finger-frame overlay)
  #:use-module (oop goops)
  #:use-module (gnome-2)
  #:use-module (gnome gtk)
  #:use-module (gnome gobject)
  #:use-module (gnome gobject generics)
  #:use-module (gnome gobject utils)
  #:use-module (gnome gw support modules)
  #:use-module (srfi srfi-11)
  ;; #:use-module (gnome gw gdk)
  #:use-module (gnome gtk gdk-event)
  #:export (<overlay>
            overlay-show
            overlay-selection-size
            overlay-selection-position))

(define-class <overlay> ()
  ;; <boolean>
  (debug-mode?
   #:accessor     overlay-debug-mode?
   #:init-keyword #:debug-mode?
   #:init-value   #f)

  (window
   #:accessor   overlay-window
   #:setter     overlay-window-set!)

  ;; <symbol>
  (state
   #:accessor   overlay-state
   #:setter     overlay-state-set!
   #:init-value 'init)

  (selection-position
   #:accessor   overlay-selection-position
   #:setter     selection-position-set!
   #:init-value #f)

  (selection-size
   #:accessor   overlay-selection-size
   #:setter     overlay-selection-size-set!
   #:init-value #f)

  (pixmap
   #:accessor   overlay-pixmap
   #:setter     overlay-pixmap-set!
   #:init-value #f))

(define-method (overlay-start-selection! (overlay <overlay>) (x <number>) (y <number>))
  (when (overlay-debug-mode? overlay)
    (format #t "overlay-start-selection!: x: ~a, y: ~a~%" x y))
  (selection-position-set! overlay (cons (inexact->exact x) (inexact->exact y))))

(define-method (handle-configuration (overlay <overlay>)
                                     (source  <gtk-drawing-area>)
                                     (event   <gdk-event>))
  (let-values (((w h) (gtk-window-get-size (overlay-window overlay))))
    (when (overlay-debug-mode? overlay)
      (format #t "handle-configuration: size: w: ~a; h: ~a~%" w h))
    (when (and (> w 1) (> h 1))
      (overlay-pixmap-set! overlay
                           (gdk-pixmap-new (get-window source)
                                           w h 24))))
  #t)

(define-method (handle-mouse-press-event (overlay <overlay>)
                                         (source  <gtk-drawing-area>)
                                         (event   <gdk-event>))
  (when (overlay-debug-mode? overlay)
    (format #t "handle-mouse-press-event: source: ~a; event: ~a~%"
            source event))
  (let-values (((ret x y) (gdk-event-get-coords event)))
    (overlay-start-selection! overlay
                              (inexact->exact x)
                              (inexact->exact y)))
  #t)

(define-method (pixmap-clear pixmap style width height)
  (gdk-draw-rectangle pixmap style #t 0 0 width height))

(define-method (handle-mouse-movement-event (overlay <overlay>)
                                            (source  <gtk-drawing-area>)
                                            (event   <gdk-event>))
  (when (overlay-debug-mode? overlay)
    (display "handle-mouse-movement-event\n"))
  (let ((position (overlay-selection-position overlay)))
    (when position
      (let-values (((ret x y) (gdk-event-get-coords event))
                   ((w h) (gtk-window-get-size (overlay-window overlay))))
        (when (overlay-debug-mode? overlay)
          (format #t "handle-mouse-movement-event: x: ~a; y: ~a~%" x y))
        (let* ((width   (round (- (inexact->exact x) (car position))))
               (height  (round (- (inexact->exact y) (cdr position))))
               (style   (gtk-widget-get-style source))
               (back-gc (get-white-gc style))
               (fg      (get-black-gc style)))
          (gdk-gc-set-background back-gc "white")
          (gdk-gc-set-background fg "red3")
          (when (and (> (abs width) 5) (> (abs height) 5))
            (when (overlay-debug-mode? overlay)
              (format #t "handle-mouse-movement-event: w: ~a; h: ~a~%"
                      width height))
            (pixmap-clear (overlay-pixmap overlay) back-gc w h)
            (gdk-draw-rectangle (overlay-pixmap overlay)
                                fg #t
                                (if (> width 0)
                                    (car position)
                                    (inexact->exact x))
                                (if (> height 0)
                                    (cdr position)
                                    (inexact->exact y))
                                (abs width)
                                (abs height))
            (gdk-draw-drawable (get-window source)
                               back-gc (overlay-pixmap overlay) 0 0 0 0
                               w h))))))
  #t)

(define-method (handle-mouse-button-release-event (overlay <overlay>)
                                                  (source  <gtk-drawing-area>)
                                                  (event   <gdk-event>))
  (let-values (((ret x y) (gdk-event-get-coords event)))
    (let* ((selection-start (overlay-selection-position overlay))
           (selection-width  (abs (inexact->exact
                                   (- x (car selection-start)))))
           (selection-height (abs (inexact->exact
                                   (- y (cdr selection-start))))))
      (overlay-selection-size-set! overlay
                                   (cons selection-width selection-height)))
    (gtk-widget-hide-all (overlay-window overlay))
    (gtk-main-quit))
  #t)


(define-method (overlay-show (overlay <overlay>))
  (let ((window (make <gtk-window>
                  #:type 'toplevel
                  #:title "finger-frame"
                  #:window-position "GTK_WIN_POS_CENTER_ALWAYS"
                  #:decorated #f
                  #:resizable #f
                  #:default-width  100
                  #:default-height 100
                  #:modal #t
                  #:opacity 0.25))
        (drawing-area (make <gtk-drawing-area>)))
    (gtk-window-maximize window)
    (gtk-container-add window drawing-area)
    (overlay-window-set! overlay window)

    ;;;; DEBUG:
    ;; (for-each (lambda (s)
    ;;             (display s)
    ;;             (newline))
    ;;           (gtype-class-get-signals <gtk-drawing-area>))

    (gtk-widget-set-events drawing-area '(exposure-mask
                                          button-press-mask
                                          button-release-mask
                                          button1-motion-mask
                                          key-press-mask))
    (gtype-instance-signal-connect drawing-area
                                   'button-press-event
                                   (lambda (source event)
                                     (handle-mouse-press-event overlay
                                                               source
                                                               event)))
    (gtype-instance-signal-connect drawing-area
                                   'motion-notify-event
                                   (lambda (source event)
                                     (handle-mouse-movement-event overlay
                                                                  source
                                                                  event)))
    (gtype-instance-signal-connect drawing-area
                                   'configure-event
                                   (lambda (source event)
                                     (handle-configuration overlay
                                                           source
                                                           event)))

    (gtype-instance-signal-connect drawing-area
                                   'button-release-event
                                   (lambda (source event)
                                     (handle-mouse-button-release-event overlay source event)))
    (show-all window)
    (gtk-main)))

